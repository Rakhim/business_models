Scalars
sellingPrice            цена продажи продукта 2                    /3.8/
highSellingPrice        обычная цена продажи продукта 1            /5.5/
middleSellingPrice      низкая цена продажи продукта 1             /5.2/
lowestSellingPrice      заниженная цена продажи продукта 1         /4/
buyPriceA               цена покупки руды A                        /3.25/
buyPriceB               цена покупки руды B                        /3.4/

baseProcessingCost      затраты на переработку                     /0.35/
convertCost             затраты на конвертацию                     /0.25/
filterCost              затраты на фильтрацию                      /0.1/

buyProductPrice1        закупочная цена тонны продукта 1           /5.75/
buyProductPrice2        закупочная цена тонны продукта 2           /4/

deliveryConstraint      ограничение снизу поставки продукта 1      /40000/
highPriceConstraint     ограничение продажи по highSellingPrice    /50000/
middlePriceConstraint   ограничение продажи по middleSellingPrice  /4000/
mineralAConstraint      ограничение поставки руды вида A           /100000/
mineralBConstraint      ограничение поставки руды вида B           /30000/
convertPowerConstraint  ограничение мощности конвертера            /50000/
basePowerConstraint     ограничение мощности процесса переработки  /100000/

convertToProduct1       коэффициент конвертаций                    /0.5/

mineralAtoProduct1      коэффициент преобразования                 /0.15/
mineralBtoProduct1      коэффициент преобразования                 /0.25/

mineralAtoProduct2      коэффициент преобразования                 /0.85/
mineralBtoProduct2      коэффициент преобразования                 /0.75/
;

Variables
z переменная целевой функций 1

xA тонн руды вида A на переработку
xB тонн руды вида B на переработку
k  тонн руды вида B на конвертацию

q1 тонн продукций вида 1 необходимой докупить
q2 тонн продукций вида 2 необходимой докупить

sh наибольшая цена продукта 1
sm средняя цена продукта 1
sl заниженная цена продукта 1;

positive variable xA;
positive variable xB;
positive variable k

positive variable q1;
positive variable q2;

positive variable sh;
positive variable sm;
positive variable sl;

Equations
object целевая функция 1

sailH            продажа по набольшей цене
sailM            продажа по средней цене
sailALL          продаем имеющееся

obtainingA       ограничение на поставку руды вида А
obtainingB       ограничение на поставку руды вида В

basePower        общая мощность основного процесса переработки
converterPower   мощность конвертера
convertConstr    конвертируем не более того что имеем
contract         ограничение снизу согласно контракту
;

object..  z =e= sellingPrice * (mineralAtoProduct2 * xA + mineralBtoProduct2 * xB - convertToProduct1 * k + q2)
 + (highSellingPrice * sh + middleSellingPrice * sm + lowestSellingPrice * sl) -
(buyPriceA * xA + buyPriceB * xB) - baseProcessingCost * (xA + xB) -
convertCost * k - filterCost * (mineralAtoProduct1 * xA + mineralBtoProduct1 * xB)
 - buyProductPrice1 * q1 - buyProductPrice2 * q2;

basePower..      xA + xB      =l= basePowerConstraint;
converterPower.. k            =l= convertPowerConstraint;
convertConstr..  k            =l= mineralAtoProduct2 * xA + mineralBtoProduct2 * xB;

obtainingA..     xA           =l= mineralAConstraint;
obtainingB..     xB           =l= mineralBConstraint;

sailH..          sh           =l= highPriceConstraint;
sailM..          sm           =l= middlePriceConstraint;

sailALL..        sh + sm + sl =l= mineralAtoProduct1 * xA + mineralBtoProduct1 * xB + convertToProduct1 * k + q1;
contract..       sh + sm + sl =g= deliveryConstraint

Model carbon /all/;
Solve carbon using lp max z;
Display  z.l, xA.l, xB.l,  k.l, q1.l, q2.l, sh.l, sm.l, sl.l;
